# Репозиторий курса Алгоритмы и структуры данных, продвинутый поток ВФТШ 2020-2021 #


**[Программа устного экзамена](https://bitbucket.org/Pelmeshek/vftsh.algo.2020/src/master/exam_program.md)**  


**[C++ style guide или как писать код:](https://bitbucket.org/Pelmeshek/vftsh.algo.2020/src/master/style_guide.md)**    
В папке **[home_tasks](https://bitbucket.org/Pelmeshek/vftsh.algo.2020/src/master/home_tasks/)** будут выкладываться домашние задания  
**[Контест с задачами №1](https://contest.yandex.ru/contest/22089/problems/)**  =

## GIT  
**[Создание ветки](https://bitbucket.org/Pelmeshek/vftsh.algo.2020/src/master/how_to_create_branches.md)**  
**[Создание пулл-реквеста](https://bitbucket.org/Pelmeshek/vftsh.algo.2020/src/master/how_to_create_pull_request.md)**  

**Полезные ссылки**:  
[Миникурс по основам с++](https://stepik.org/course/16480/syllabus)  
[Изучение git в игровой форме](https://learngitbranching.js.org/). Здесь задачи могут попасьтся сложнее требуемого в вфтш уровня, но штука интересная  